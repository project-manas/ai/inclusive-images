import tensorflow as tf
from tensorflow.keras import backend as K
from tensorflow.keras.applications import Xception
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Dense, GlobalAveragePooling2D

import csv
import numpy as np
import matplotlib.image as mpimg
from os import listdir
import time
import h5py
import random
import math


batch_size = 16

lr = 0.0005

freqs = []
imagesList = listdir('dataset/train_299x299/')

with open('freq_16_sorted.csv', 'r') as sort:
	freq = csv.reader(sort, delimiter=',')
	for i, j in enumerate(freq):
		freqs.append(int(float(j[0])))

def _parse_function(image_file):
	image_string = tf.read_file(image_file)
	image_decoded = tf.image.decode_jpeg(image_string, channels = 3)
	image = tf.cast(image_decoded, tf.float32)
	image = tf.reshape(image, (299, 299, 3))
	return image

imagedata = ['dataset/train_299x299/'+x for x in imagesList]
labeldata = ['labels/'+x+'.csv' for x in imagesList]
imageset = tf.data.Dataset.from_tensor_slices((imagedata))
imageset = imageset.map(_parse_function)
imageset = imageset.batch(batch_size)
imageset = imageset.prefetch(5)
imageset = imageset.repeat()
iter_img = imageset.make_one_shot_iterator()
# labelset = tf.data.Dataset.from_tensor_slices((labeldata))
# labelset = labelset.map(_parse_function_label)
record_defaults = [[0.0]] * 100
labelset = tf.contrib.data.CsvDataset(labeldata, record_defaults, select_cols = freqs)
labelset = labelset.batch(batch_size)
labelset = labelset.prefetch(5)
labelset = labelset.repeat()
iter_label = labelset.make_one_shot_iterator()

# record_defaults = [[0]] * 100
# labelset = tf.contrib.data.CsvDataset([x+'.csv' for x in imagesList], record_defaults, select_cols = freqs)

# dataset = tf.data.Dataset.zip((imageset, labelset)).batch(batch_size)

inputs = Input(tensor = iter_img.get_next(), shape= (299, 299, 3))

# create the base model
base_model = Xception(
    include_top=False,
    weights=None,
    input_tensor=inputs,
    input_shape=(299, 299, 3),
    pooling=None,
    classes=100
)

# add a global spatial average pooling layer
x = base_model.output
x = GlobalAveragePooling2D()(x)
# let's add a fully-connected layer
x = Dense(1024, activation='relu')(x)
# and a logistic layer -- we have 7178 classes
predictions = Dense(100, activation='sigmoid')(x)

# this is the model we will train
model = Model(inputs=inputs, outputs=predictions)

# first: train only the top layers (which were randomly initialized)
# i.e. freeze all convolutional Exception layers
for layer in base_model.layers:
    layer.trainable = True

# parallel_model = multi_gpu_model(model, gpus= 3)

optimizer= tf.train.AdamOptimizer(learning_rate=lr)

def loss_fn(targets, logits, pos_weight = 10):
	targets = tf.transpose(targets)
	# targets = tf.Print(targets, [targets], summarize = (batch_size*100))
	return tf.nn.weighted_cross_entropy_with_logits(targets, logits, pos_weight)

# compile the model (should be done *after* setting layers to non-trainable)
model.compile(optimizer=optimizer, loss=loss_fn, target_tensors = [iter_label.get_next()])

class_weights = dict()
with open('freq_16_sorted.csv', 'r') as file:
	reader = csv.reader(file)
	for i,j in enumerate(reader):
		if not int(j[1]):
			class_weights[i] = 0
		else:
			class_weights[i] = 8-int(j[1])

def test(model, epoch):
    imagesList = listdir('dataset/train_299x299/')
    with open('predictions'+str(epoch)+'.csv', 'a') as file:
	    for image in imagesList:
	        batch_input = [mpimg.imread('dataset/train_299x299/'+image)]
	        batch_x = np.array(batch_input)
	        pred = model.predict(batch_x)
	        file.write(image+'\t')
	        np.savetxt(file, pred, delimiter=',')

class Prediction(tf.keras.callbacks.Callback):
	def on_train_begin(self, logs={}):
		return

	def on_train_end(self, logs={}):
		return

	def on_epoch_begin(self, epoch, logs={}):
		return

	def on_epoch_end(self, epoch, logs={}):
		global lr
		if(epoch % 99 == 0):
			lr /= 2
		# tf.keras.callbacks.ModelCheckpoint('weights{epoch:08d}.h5', save_weights_only = True, period = 1)
		# test(self.model, epoch)
		# np.savetxt('predictions'+str(epoch)+'.csv', predictions, delimiter=',')
		self.model.save_weights('weights_e'+str(epoch)+'.h5')
		return

	def on_batch_begin(self, batch, logs={}):
		return

	def on_batch_end(self, batch, logs={}):
		return

# tb = tf.keras.callbacks.TensorBoard(log_dir='./logs', histogram_freq=0, batch_size=16, write_graph=True, write_grads=False, write_images=False)

pred = Prediction()

# datagen = image_generator()

model.fit(steps_per_epoch=1, epochs=1000, verbose=1, callbacks=[pred], class_weight = class_weights)
# # model.save_weights('my_model_weights.h5')
# predictions = parallel_model.predict(test('test/'))
# np.savetxt('predictions.csv', predictions, delimiter=',')
# print(predictions)