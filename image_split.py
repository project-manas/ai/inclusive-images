import numpy as np
import pandas as pd
import cv2
import os

def load_images_from_folder(folder):
	for filename in os.listdir(folder):
		img=cv2.imread(os.path.join(folder,filename))
		if img is not None:
			h=img.shape[0]
			w=img.shape[1]
			if((h-720)**2<=(h-1024)**2):
				if((w-720)**2<=(w-1024)**2):
					resize_img=cv2.resize(img,(720,720),interpolation=cv2.INTER_AREA)
					cv2.imwrite('train_mod_720x720/'+filename,resize_img)
				else:
					resize_img=cv2.resize(img,(720,1024),interpolation=cv2.INTER_AREA)
					cv2.imwrite('train_mod_720x1024/'+filename,resize_img)
			elif((w-720)**2<=(w-1024)**2):
				resize_img=cv2.resize(img,(1024,720),interpolation=cv2.INTER_AREA)
				cv2.imwrite('train_mod_1024x720/'+filename,resize_img)
			else:
				resize_img=cv2.resize(img,(1024,1024),interpolation=cv2.INTER_AREA)
				cv2.imwrite('train_mod_1024x1024/'+filename,resize_img)

load_images_from_folder('train_0')
