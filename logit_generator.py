import csv
label = 'train_machine_labels.csv'
label_code = 'classes-trainable.csv'
file = open("logits.csv", "w")
with open(label, 'r') as csvfile:
	readCSV = csv.reader(csvfile, delimiter=',')
	temp = ""
	labels = []
	for row in readCSV:
		if not row[0] == temp:
			file.write(temp)
			logits = [0] * 7178
			for x in labels:
				index = 0
				with open(label_code, 'r') as csvcodefile:
					readCSVCode = csv.reader(csvcodefile, delimiter=',')
					for code in readCSVCode:
						if code[0] == x[0]:
							logits[index] = x[1]
							break
						index = index + 1
			labels = []
			for x in logits:
				file.write(",")
				file.write(str(x))
			temp = row[0]
			file.write('\n')
		labels.append([row[2], row[3]])


