import pandas as pd
import numpy as np
df = pd.read_csv('logits_human.csv')
summ = df.drop(df.columns[0], axis = 1)
summ = summ.sum(axis = 0)
index = [x for x in range(7178)]
arr = np.vstack([index, summ])
arr = np.transpose(arr)
arr = arr*-1
arr = arr[arr[:,1].argsort()]
arr = arr*-1
# sort = summ.sort_values(ascending=False)
# sort.to_csv('freq_sorted.csv')
np.savetxt('freq_sorted1.csv', arr, delimiter = ',')